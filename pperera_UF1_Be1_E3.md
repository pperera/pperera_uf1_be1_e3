# UF1. Programació estructurada.	


|Material de consulta:|<p>**Teoria:**</p><p>- UF1 - T1 - Estructura d'un programa informàtic.</p><p>- UF1 - T2 - Programació estructurada</p><p>**Annexos:**</p><p>- UF1 - T2 - Taules i cadenes en C.</p><p>- UF1 - T1i2 - Annex E3 (*scanf* correcte)</p><p>- UF1 - T2 - Annex E3 (tipus booleà en C)</p><p>- UF1 -T1I2 - Annex E3 àmbit de les variables</p><p>- **Explicacions de classe.**</p>|
| :- | :- |
|Format del lliurament:|<p>**nomUsuari\_UF1\_Be1\_E3**</p><p>Per exemple si l’usuari és *asanchez* el nom seria “*asanchez \_UF1\_Be1\_E3*”</p><p>Les respostes feu-les en color blau.</p>|
|Durada:|Establerta al portal web de l’assignatura.|

Notes:

- ***Els exercicis s’han de codificar en ANSI C.***
- ***Afegiu a cada exercici una captura de la terminal del sistema que demostri la correctesa de la seva execució.***

## 1. Lectura i escriptura de dades.

### Exercici 1: Missatge benvinguda	 Escriu un missatge de benvinguda per pantalla.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    printf("Benvingut a les classes de programacio");

    return 0;

}
```




![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.001.png)

### Exercici 2: Tipus simples	 Escriu un enter, un real, i un caràcter per pantalla.
```
#include <stdio.h>

#include <stdlib.h>

int main(){

        int enter =1;

        float real;

        real=2.1415;

        char caracter='a';

        printf("enter: %d\n",enter);

        printf("numero real: %f\n",real);

        printf("caracter: %c",caracter);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.002.png)

### Exercici 3: Suma enters.	 Demana dos enters per teclat i mostra la suma per pantalla.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        int enter1;

        int enter2;

        int resultat;

        printf("Introdueix el numero 1\n");

        scanf("%d",&enter1);

        printf("Introdueix el numero 2\n");

        scanf("%d",&enter2);

        resultat=enter1+enter2;

        printf("La suma del numero 1 que has introduit %d i del numero 2 que has escollit %d el resultat es %d",enter1,enter2,resultat);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.003.png)

### Exercici 4: Tipus.	                                                                                Indica el tipus de dades que correspon en cada cas:

4.1 Edat ->  Enter int

4.2 Temperatura -> Real float o double

4.3 La resposta a la pregunta: Ets solter (s/n)? -> boolear

4.4 El resultat d’avaluar la següent expressió: (5>6 o 4<=8) -> condicional

4.5 El teu nom. -> Cadena



### Exercici 5: Números.	 Mostrar per pantalla els 5 primers nombres naturals.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        int enter1=1;

        int enter2=2;

        int enter3=3;

        int enter4=4;

        int enter5=5;

        printf("Els primer 5 numero naturals son: %d %d %d %d %d",enter1,enter2,enter3,enter4,enter5);


        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.007.png)


### Exercici 6: Suma.	 Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        int enter1=1;

        int enter2=2;

        int enter3=3;

        int enter4=4;

        int enter5=5;

        int resultat;

        int resultat2;

        resultat=enter1+enter2+enter3+enter4+enter5;

        resultat2=enter1\*enter2\*enter3\*enter4\*enter5;

        printf("La suma dels 5 primer numeros naturals és %d\n",resultat);

        printf("El producte dels 5 primer numeros naturals és %d\n",resultat2);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.008.png)

### Exercici 7: Intercanvi.	 Demana dos enters i intercanvia els valors de les variables.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        int enter1;

        int enter2;

        int aux;

        printf("Introudeix el primer enter\n");

        scanf("%d",&enter1);

        printf("Introudeix el segon enter\n");

        scanf("%d",&enter2);

        printf("Els numeros introduits son %d i %d\n",enter1,enter2);

        aux=enter1;

        enter1=enter2;

        enter2=aux;

        printf("Els numeros girats son %d i %d\n",enter1,enter2);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.009.png)

### Exercici 8: Pantalla de menú	 Mostrar per pantalla el següent menú:
Opcions:

1-Alta 

2-Baixa

3-Modificacions 

4-Sortir

Tria opció (1-4):

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        printf("Opcion:\n");

        printf("\t 1-Alta\n");

        printf("\t 2-Baixa\n");

        printf("\t 3-Modificacions\n");

        printf("\t 4-Sortir\n");

        printf("Tria opcio (1-4):");

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.010.png)

### Exercici 9: Pantalla de menú	 Mostra per pantalla el següent menú:

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

\*\*\*\*	MENU	\*\*\*\*

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

\*\*\*\*	1-alta	\*\*\*\*

\*\*\*\*	2-baixa	\*\*\*\*

\*\*\*\*	2-sortir	\*\*\*\*

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

     printf("\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\n");

        printf("\*\*\*\*  \t MENU   \t\*\*\*\*\n");

        printf("\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\n");

        printf("\*\*\*\*\t 1-alta \t\*\*\*\*\n");

        printf("\*\*\*\*\t 2-baixa\t\*\*\*\*\n");

        printf("\*\*\*\*\t 2-sortir\t\*\*\*\*\n");

        printf("\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*");

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.011.png)



### Exercici 10: Càlcul del capital final en interès simple	 Feu un programa que resolgui el següent enunciat:

Calcular el Capital Final **CF** d’una inversió, a interès simples, si sabem el Capital Inicial (**C0**), el nombre d’anys (**n**), i el rèdit (**r**). La fórmula per obtenir el CF a interès simples és:


![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.012.png)

Donat que el temps està expressat en anys la **k=1**. El rèdit ‘**r**’ ve donat en **%**.

Nota: les dades de l’exercici cal demanar-les per teclat.
```
#include <stdio.h>

#include <stdlib.h>

int main()

{

        int capitalInicial;

        int anys;

        float redit;

        float resultat;

        float percentatge;

        printf("introdueix el capital inicial\n");

        scanf("%d",&capitalInicial);

        printf("introdueix els anys\n");

        scanf("%d",&anys);

        printf("introdueix el redit\n");

        scanf("%f",&redit);

        percentatge=redit/100;

        resultat=(capitalInicial\*percentatge\*anys);

        printf("El resultat del capital final en interes simple es: %.2f",resultat);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.013.png)




### Exercici 11: Càlcul del Capital final en interès compost	 Feu un programa que resolgui el següent enunciat:

Calcular el Capital Final **CF** d’una inversió, a interès compost, si sabem el Capital Inicial (**C0**), el nombre d’anys (**n**), i el interès (**i**). La fórmula per obtenir el CF a interès compost és:

![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.016.png)

L’ interès ‘**i**’ ve donat en tant per **1**.

Nota: les dades de l’exercici cal demanar-les per teclat.
```
#include <stdio.h>

#include <stdlib.h>

#include <math.h>

int main()

{

        int capitalInicial;

        int anys;

        float interes;

        float resultat;

        printf("introdueix el capital inicial\n");

        scanf("%d",&capitalInicial);

        printf("introdueix els anys\n");

        scanf("%d",&anys);

        printf("introdueix el interes\n");

        scanf("%f",&interes);

        resultat=capitalInicial\*(pow((1+interes), anys));

        printf("El resultat del capital final en interes compost es: %.2f",resultat);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.017.png)


### Exercici 12: La travessa	 Feu un programa que permeti generar una travessa de forma aleatòria. Tot seguit ha de permetre introduir-ne una per teclat i comprovar-ne els encerts respecte la travessa generada aleatòriament. El format de sortida queda a la vostra elecció sempre que n’informi dels encerts.
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define BB while (getchar()!='\n')

int main()
{
        int contador=0;
        int primer;
        int segon;
        int tercer;
        int quarta;
        int cinque;
        int sisena;
        int setena;
        int vuitena;
        int novena;
        int desena;
        int onzena;
        int dotzena;
        int tretzena;
        int catorzena;

        int opcio1=rand()%2+1;
        int opcio2=rand()%2+1;
        int opcio3=rand()%2+1;
        int opcio4=rand()%2+1;
        int opcio5=rand()%2+1;
        int opcio6=rand()%2+1;
        int opcio7=rand()%2+1;
        int opcio8=rand()%2+1;
        int opcio9=rand()%2+1;
        int opcio10=rand()%2+1;
        int opcio11=rand()%2+1;
        int opcio12=rand()%2+1;
        int opcio13=rand()%2+1;
        int opcio14=rand()%2+1;

        printf("Endevina la travessa\n");
        printf("Introdueix 1 per al primer equip 0 per al empat o 2 per al segon equip\n");
        printf("Primer partit elegeix (1,0,2)\n");
        scanf("%d",&primer);
        BB;
        printf("Segon partit elegeix (1,0,2)\n");
        scanf("%d",&segon);
        BB;
        printf("Tercer partit elegeix (1,0,2)\n");
        scanf("%d",&tercer);
        BB;
        printf("Quarta partit elegeix (1,0,2)\n");
        scanf("%d",&quarta);
        BB;
        printf("Cinque partit elegeix (1,0,2)\n");
        scanf("%d",&cinque);
        BB;
        printf("Sise partit elegeix (1,0,2)\n");
        scanf("%d",&sisena);
        BB;
        printf("Sete partit elegeix (1,0,2)\n");
        scanf("%d",&setena);
        BB;
        printf("Vuite partit elegeix (1,0,2\n)");
        scanf("%d",&vuitena);
        BB;
        printf("Novena partit elegeix (1,0,2\n)");
        scanf("%d",&novena);
        BB;
        printf("Dese partit elegeix (1,0,2\n)");
        scanf("%d",&desena);
        BB;
        printf("Onze partit elegeix (1,0,2\n)");
        scanf("%d",&onzena);
        BB;
        printf("Dotze partit elegeix (1,0,2\n)");
        scanf("%d",&dotzena);
        BB;
        printf("Tretze partit elegeix (1,0,2\n)");
        scanf("%d",&tretzena);
        BB;
        printf("Catorze partit elegeix (1,0,2\n)");
        scanf("%d",&catorzena);
        BB;
        if((primer==opcio1) && (segon==opcio2) && (tercer==opcio3) && (quarta==opcio4) && (cinque==opcio5) && (sisena==opcio6) && (setena==opcio7) && (vuitena==opcio8) && (novena==opcio9) && (desena==opcio10) && (onzena==opcio11) && (dotzena==opcio12) && (tretzena==opcio13) && (catorzena==opcio14)){

          printf("Enhorabona els has encertat tots");
        }
        else {
          if(primer==opcio1) contador++;
          else if(segon==opcio2) contador++;
          else if(tercer==opcio3) contador++;
          else if(quarta==opcio4) contador++;
          else if(cinque==opcio5) contador++;
          else if(sisena==opcio6) contador++;
          else if(setena==opcio7) contador++;
          else if(vuitena==opcio8) contador++;
          else if(novena==opcio9) contador++;
          else if(desena== opcio10) contador++;
          else if(onzena== opcio11) contador++;
          else if(dotzena==opcio12) contador++;
          else if(tretzena==opcio13) contador++;
          else if(catorzena==opcio14)contador++;
          printf("Has encertat %d partits",contador);

        }

        return 0;
}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.018.png)





### Exercici 13: Joc dels daus	 Feu un programa que permeti simular el resultat de tirar un dau. Tot seguit ha de demanar un número per teclat, entre 1 i 6, i ens ha dir si és el mateix que el que hem generat aleatòriament.
```
#include <stdio.h>

#include <stdlib.h>

#include <unistd.h>

#define BB while (getchar()!='\n')

int main()

{

        int numero=rand()%6+1;

        int numeroElegit;

        printf("Tirare un dau de 6 cares i tindras de endevinar el numero\n");

        scanf("%d",&numeroElegit);

        BB;

        if(numero==numeroElegit){

            printf("L'has endevinat tu has elegit %d i ha sortit el %d", numeroElegit, numero);

        }else printf("No l'has encertat el numero ere %d",numero);

        return 0;

}
```
![](Aspose.Words.69f703d9-a571-4083-8da1-ff38d914f797.019.png)
